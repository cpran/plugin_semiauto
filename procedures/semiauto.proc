#! ## `trace` {#trace}
#!
#! ~~~ params
#! in:
#!   .msg$: A string to print to trace device
#!   trace: A global trace flag. If false, this procedure does nothing
#!   trace$: >
#!     The filename of an external trace file. If empty, print trace to STODUT.
#! ~~~
#!
#! Print a trace message, either to STDOUT or to a file, if a trace option
#! has been set (by setting `trace` to `1`). If the flag has been set to
#! true, the value of `trace$` is used as the filename of a file to write
#! to. If that variable is the empty string, the message is printed to
#! the Info Window.
#!
procedure trace: .msg$
  if trace
    if trace$ != ""
      appendFileLine: d.output_directory$ + trace$, .msg$
    else
      appendInfoLine: .msg$
    endif
  endif
endproc

#! ## `find_first_unpaired` {#find-first-unpaired}
#!
#! ~~~ params
#! out:
#!   .i: Index of first unpaired object, 0 if all are paired.
#! selection:
#!   in:
#!     strings: 2
#! ~~~
#!
#! Find the first sound file for which no Pitch object exists (see
#! [pitch_exists](#pitch-exists)).
#!
#! If no unpaired object is found, the script exits cleanly (since there is
#! no more work to do).
#!
procedure find_first_unpaired ()
  .alist = selected("Strings", 1)
  .blist = selected("Strings", 2)

  selectObject: .alist
  .astrings = Get number of strings

  selectObject: .blist
  .bstrings = Get number of strings

  if .bstrings > .astrings
    .tmp = .alist
    .alist = .blist
    .blist = .tmp

    .tmp = .astrings
    .astrings = .bstrings
    .bstrings = .tmp
  endif

  .i = 0
  repeat
    .i += 1
    selectObject: .alist
    .aitem$ = Get string: .i

    selectObject: .alist
    @findInStrings: replace_regex$(.aitem$, "\.[^.]+$", "", 0), 0

    .exists = findInStrings.return
  until .i == .astrings or !.exists

  if .exists
    @clean_exit: "No unpaired objects."
  else
    for_each.start_from = .i
  endif
endproc

#! ## `find_marks` {#find-marks}
#!
#! ~~~ params
#! out:
#!   .table: ID of Table object with data from matching marks (see below).
#! ~~~
#!
#! Find the intervals or points (the "marks") matching the [regular
#! expression](#arguments) passed as argument to the script.
#!
#! For each mark, the index and label are stored in a table. Additionally,
#! timestamps are saved for the start, midpoint and end of intervals. In
#! the case of points, the time of the point is saved as the midpoint, and
#! the start and end depend on the magnitude of the
#! [time span](#arguments).
#!
procedure find_marks ()
  if d.use_textgrids
    .table = Create Table with column names: "valid_marks", 0,
      ... "index label start mid end"

    if d.label_regex$ != ""
      selectObject: l.textgrid
      if l.interval_marks
        .marks = Get number of intervals: d.tier
        for .i to .marks
          selectObject: l.textgrid
          .label$ = Get label of interval: d.tier, .i
          if index_regex(.label$, d.label_regex$)
            .start = Get start point: d.tier, .i
            .end   = Get end point:   d.tier, .i
            selectObject: .table
            Append row
            .row = Object_'.table'.nrow
            Set numeric value: .row, "index", .i
            Set string value:  .row, "label", .label$
            Set numeric value: .row, "start", .start
            Set numeric value: .row, "mid",   .start + ((.end - .start) / 2)
            Set numeric value: .row, "end",   .end
          endif
        endfor
      else
        .marks = Get number of points: d.tier
        for .i to .marks
          selectObject: l.textgrid
          .label$ = Get label of point: d.tier, .i
          if index_regex(.label$, d.label_regex$)
            .time = Get time of point: d.tier, .i
            selectObject: .table
            Append row
            .row = Object_'.table'.nrow
            Set numeric value: .row, "index", .i
            Set string value:  .row, "label", .label$
            Set numeric value: .row, "start", .time - (d.span / 2)
            Set numeric value: .row, "mid",   .time
            Set numeric value: .row, "end",   .time + (d.span / 2)
          endif
        endfor
      endif
    endif
  endif
endproc

#! ## `split_time` {#split-time}
#!
#! ~~~ params
#! in:
#!   .start: The start point of the interval
#!   .end: The end point of the interval
#!   .n: The number of timestamps to find
#! out:
#!   .time[]: The list of evenly spaced timestamps
#! ~~~
#!
#! Finds `.n` evenly spaced time stamps between `.start` and `.end`.
#!
procedure split_time: .start, .end, .n
  for .i to .n
    .time[.i] = (.end - .start) / (1 + .n) * .i + .start
  endfor
endproc

#! ## `initial_defaults` {#initial-defaults}
#!
#! ~~~ params
#! out: Modifies values in the default namespace
#! ~~~
#!
#! Initialises variables in the default namespace to their base defaults
#!
procedure initial_defaults ()
  @_initialise_defaults()
  @set_defaults ()
endproc

#! ## `set_defaults` {#set-defaults}
#!
#! ~~~ params
#! out: Modifies values in the default namespace
#! ~~~
#!
#! Updates string and numeric variables in the default namespace to their
#! new values based on the default values of other variables
#!
procedure set_defaults ()
  @_string_defaults()
  @_numeric_defaults()
endproc

procedure relative_or_absolute: .dir$, .ref$
#   .dir$ = .dir$ + if right$(.dir$) != "/" then "/" else "" fi
#   .ref$ = .ref$ + if right$(.ref$) != "/" then "/" else "" fi

  if left$(.dir$) == "/" or
    ... (windows and index_regex(.dir$, "^[a-zA-Z]:"))
    # Full path
    .return$$ = .dir$
  else
    # Relative path
    .return$ = .ref$ + .dir$
  endif
endproc
