# Semi-auto Pitch object wizard
#
# This script is heavily inspired from Dan McCloy's Semi-auto Pitch Extractor,
# although it has been almost entirely rewritten to make use of the new syntax
# as well as the object-cycling features implemented in the "vieweach" CPrAN
# plugin.
#
# Provided with a directory with sound files, the script will loop through each
# one and provide an interface to adjust the pitch detection parameters used by
# Praat. Optionally, the user may choose to open an accompanying TextGrid
# annotation to facilitate navigation.
#
# Unlike McCloy's original script, this one is not designed to be run on long
# files, and the ideal sound it will have to deal with will contain a single
# utterance. The main effect this design difference has (and the main difference
# these two scripts have) is that this one does not deal with zoom levels at
# all, leaving that to the user.
#
# Another difference is the approach taken for the manual editing of Pitch
# objects. In the case of this script, this is done by providing the user with
# the chance to enter the Pitch editor for the current Sound object, using the
# parameters that are chosen at that time. As an added feature, these parameters
# (specifically, pitch floor and ceiling) can be automatically estimated from
# the current utterance by means of Hirst and de Looze's two-pass
# approach [1,2].
#
# The initial version of this script was developed with support from University
# of Chicago.
#
# Version 0.0.1 - first working version
# Author: José Joaquín Atria
#
# [1] De Looze and Hirst, 2008. "Detecting changes in key [...]", Speech Prosody
# [2] Hirst, 2011. "The analysis by synthesis [...]", JoSS 1(1): 55-83
#
# This script is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.

include ../../plugin_utils/procedures/check_directory.proc
include ../../plugin_twopass/procedures/pitch_two-pass.proc
include ../../plugin_strutils/procedures/find_in_strings.proc

preferencesDirectory$ = replace_regex$(preferencesDirectory$, "(con)?(\.(EXE|exe))?$", "", 0)

strutils$  = preferencesDirectory$ + "/plugin_strutils/scripts/"
selection$ = preferencesDirectory$ + "/plugin_selection/scripts/"

form Semi-auto pitch detection...
  sentence Sound_directory
  sentence TextGrid_directory
  sentence Output_directory
  boolean  Use_TextGrids 0
  real     left_Default_pitch_range_(Hz)  75
  real     right_Default_pitch_range_(Hz) 600
  comment  Set either to 0 for automatic per-utterance estimation
  boolean  Keep_object_table 0
endform

# Set default values
default.pitch_floor   = left_Default_pitch_range
default.pitch_ceiling = right_Default_pitch_range

# Use GUI for sound directory selection if not manually provided
@checkDirectory(sound_directory$, "Read Sounds from...")
sound_path$ = checkDirectory.name$

# Generate a sound list using full paths
runScript: strutils$ + "file_list_full_path.praat", 
  ... "sounds_fullpath", sound_path$, "*wav", 1
sound_basenames = selected(1)
sound_list      = selected(2)

selectObject: sound_basenames
Rename: "sound_basenames"

selectObject: sound_list
Rename: "sound_list"
total_sounds = Get number of strings

# If we are using TextGrids, process them as well
if use_TextGrids
  @checkDirectory(textGrid_directory$, "Read TextGrids from...")
  textgrid_path$ = checkDirectory.name$

  # Generate an annotation list using full paths
  runScript: strutils$ + "file_list_full_path.praat", 
    ... "textgrids_fullpath", textgrid_path$, "*TextGrid", "no"
  textgrid_list = selected()
  Rename: "textgrid_list"
endif

# Provide a GUI selector for the output directory
@checkDirectory(output_directory$, "Save Pitch objects to...")
pitch_path$ = checkDirectory.name$

# Read existing Pitch objects from that directory. This will later be used to
# check whether a specific Sound has a Pitch object associated with it
pitch_basenames = Create Strings as file list: "pitchs",
  ... pitch_path$ + "*Pitch"
total_pitchs = Get number of strings

if total_pitchs
  beginPause: "Pitch objects found"
  comment: "The output directory already contains some Pitch objects"
  comment: "Do you want to continue from the first unpaired Sound?"
  button = endPause: "Cancel", "Continue", "From start", 2, 1
  if button = 1
    @cleanUp()
    exit
  elsif button = 2
    @findFirstUnpaired()
  elsif button = 3
    for_each.start_from = 1
  endif
else
  for_each.start_from = 1
endif

# Initialise the object list, which will keep information about processed files
object_list = Create Table with column names: "object_list", 0,
  ... "name floor ceiling pitch notes"

# Procedure overrides
#
# The object cycling features in "vieweach" provide hooks to specify code to be
# executed at specific points during each iteration. In order to inject that
# code, specific procedures have to be locally defined before we include the
# main procedure definitions.
#
# Some internal variables are used (they start with "view_each", but care must
# be used not to modify them, which might have unexpected results.

# Executes at each iteration, before the editor opens
procedure view_each.before_editor ()
  local.sound  = selected("Sound")
  local.sound$ = selected$("Sound")
  
  selectObject: object_list
    
  .exists = Search column: "name", local.sound$
  if !.exists
    local.pitch_floor   = default.pitch_floor
    local.pitch_ceiling = default.pitch_ceiling

    if !local.pitch_floor or !local.pitch_ceiling
      @twoPass(sound)
    endif

    selectObject: object_list
    Append row
    Set string value: Object_'object_list'.nrow, "name", local.sound$
  else
    .row = .exists
    local.pitch_floor   = Object_'object_list'[.row, "floor"]
    local.pitch_ceiling = Object_'object_list'[.row, "ceiling"]
  endif

  @pitchExists(local.sound$ + ".Pitch")
  local.has_pitch = pitchExists.return
  local.pitch$ = pitchExists.return$
  
  selectObject: for_each.selection
  runScript: selection$ + "restore_selection.praat"
endproc

# Executes at the beginning of the editor window, for each iteration
procedure view_each.at_begin_editor ()
  Show analyses: "yes", "yes", "no", "no", "no", 10
  Spectrogram settings: 0, 2000, 0.025, 50
  Advanced spectrogram settings: 1000, 250,
    ... "Fourier", "Gaussian", "yes", 100, 6, 0
  Pitch settings: local.pitch_floor, local.pitch_ceiling,
    ... "Hertz", "autocorrelation", "automatic"
  Advanced pitch settings: 0, 0, "no", 15, 0.03, 0.45, 0.01, 0.35, 0.14
endproc

# Defines the pause that occurs at each iteration
# This is where most of the logic for this script is defined.
procedure view_each.pause ()
  label SEMIAUTO_PAUSE_START
  repeat
    selectObject: object_list
    .object_row = Search column: "name", local.sound$

    if local.has_pitch
      Set string value: .object_row, "pitch", local.pitch$
    elsif !local.pitch_floor or !local.pitch_ceiling
      @twoPass(local.sound)
    endif

    beginPause: "Adjust pitch analysis settings"
    comment: "File " + local.sound$ + " " +
      ... "(" + string$(for_each.current) + " of " + string$(for_each.last) + ")"
    if local.has_pitch
      comment: "There is already a Pitch object for this file. " +
        ... "Press Edit to view."
    else
      comment: "You can change the pitch settings " +
        ... "if the pitch track doesn't look right."
    endif
    real: "Pitch_floor", local.pitch_floor
    real: "Pitch_ceiling", local.pitch_ceiling
    boolean: "Set as default", 0
    sentence: "Notes", ""
    if local.has_pitch
      comment: "Press Reset to ignore existing Pitch object"
    endif

    .stop = 1
    if for_each.current > 1
      .button = endPause: "Stop", "<",
        ... if local.has_pitch then "Reset" else "Redraw" fi, "Edit",
        ... if for_each.current = for_each.last then "Finish" else ">" fi, 3, 1
      .back       = 2
      .redraw     = 3
      .edit_pitch = 4
      .forward    = 5
    else
      .button = endPause: "Stop",
        ... if local.has_pitch then "Reset" else "Redraw" fi, "Edit",
        ... if for_each.current = for_each.last then "Finish" else ">" fi, 2, 1
      .back       = 0
      .redraw     = 2
      .edit_pitch = 3
      .forward    = 4
    endif

    if .button != .stop
      local.pitch_floor   = pitch_floor
      local.pitch_ceiling = pitch_ceiling

      if set_as_default
        default.pitch_floor   = local.pitch_floor
        default.pitch_ceiling = local.pitch_ceiling
      endif

      if !local.pitch_floor or !local.pitch_ceiling
        @twoPass(local.sound)
      endif

      if .button = .redraw
        local.has_pitch = 0
        editor: view_each.editor$
          Pitch settings: local.pitch_floor, local.pitch_ceiling,
            ... "Hertz", "cross-correlation", "automatic"
        endeditor
      endif
    endif

  until .button != .redraw

  if .button != .stop
    selectObject: object_list
    Set numeric value: .object_row, "floor",   local.pitch_floor
    Set numeric value: .object_row, "ceiling", local.pitch_ceiling
    Set string value:  .object_row, "notes",   notes$

    .pitch = undefined
    if !local.has_pitch and (.button = .forward or .button = .edit_pitch)
      selectObject: local.sound
      .pitch = To Pitch: 0, local.pitch_floor, local.pitch_ceiling
    endif

    if .button = .edit_pitch
      if local.has_pitch
        .pitch = Read from file: pitch_path$ + local.sound$ + ".Pitch"
      endif
      selectObject: .pitch
      .pitch$ = selected$()
      View & Edit
      beginPause: "Edit Pitch object"
      comment: "Press OK to accept and continue, or Cancel to go back"
      .pitch_button = endPause: "Cancel", "OK", 2, 1
      if .pitch_button = 1
        nocheck editor: pitch$
          nocheck Close
        nocheck endeditor
        selectObject: .pitch
        Remove
        goto SEMIAUTO_PAUSE_START
      else
        .button = .forward
      endif
    endif

    if .button = .forward and .pitch != undefined
      selectObject: .pitch
      .pitch$ = selected$("Pitch")
      .pitch_filename$ = .pitch$ + ".Pitch"
      if fileReadable(pitch_path$ + .pitch_filename$)
        beginPause: "File exists. Overwrite?"
        .overwrite = endPause: "No", "Yes", 1, 1
        .overwrite -= 1
      else
        .overwrite = 1
      endif
      if .overwrite
        Save as text file: pitch_path$ + .pitch_filename$
      endif
      Remove

      @pitchExists(.pitch_filename$)
      if !pitchExists.return
        selectObject: pitch_basenames
        Insert string: 1, .pitch_filename$
      endif

      selectObject: object_list
      Set string value: .object_row, "pitch", .pitch$
    endif
  endif

  # The for_each() procedure in "vieweach" expects a .next variable with the item
  # to be shown next. This variable can have one of three possible values:
  #   * -1, to go to the previous object
  #   * +1, to go to the next object
  #   *  0, to exit
  if .button = .stop
    # Pressed stop
    for_each.next = 0
  elsif .button = .back
    # Pressed back
    for_each.next = -1
  elsif .button = .forward
    # Pressed forward
    for_each.next = if for_each.current = for_each.last then 0 else 1 fi
  endif
endproc

# Include the main procedures. Local overrides must be defined before this line
include ../../plugin_vieweach/procedures/view_each.proc

# Select the object lists and call the main iterating procedure
selectObject: sound_list
if use_TextGrids
  plusObject: textgrid_list
endif
@view_each()

# Clean up.
@cleanUp()

if keep_object_table
  selectObject: object_list
endif

# Local procedures
# These procedures are only used for this script

procedure cleanUp ()
  nocheck removeObject: files
  if !keep_object_table
    nocheck removeObject: object_list
  endif
  nocheck removeObject: textgrid_list
  nocheck removeObject: sound_list
  nocheck removeObject: sound_basenames
  nocheck removeObject: pitch_basenames
endproc

# Estimate pitch floor and ceiling values from utterance
procedure twoPass (.id)
  @in_editor()
  if in_editor.return
    .editor_name$ = extractLine$(.info$, "Editor name: ")
    .editor_name$ = extractLine$(.info$, ". ")
    endeditor
  endif
  
  selectObject: .id
  @pitchTwoPass(0.75, 1.5)
  
  Remove
  
  local.pitch_floor   = pitchTwoPass.floor
  local.pitch_ceiling = pitchTwoPass.ceiling
  
  if in_editor.return
    editor: .editor_name$
  endif
endproc

# Check whether a Pitch object with that name exists
procedure pitchExists (.name$)
  selectObject: pitch_basenames
  @findInStrings: .name$, 0
  .return = findInStrings.return
  .return$ = Get string: .return
endproc

procedure findFirstUnpaired ()
  .i = 0

  repeat
    .i += 1
    selectObject: sound_basenames
    .sound$ = Get string: .i
    selectObject: pitch_basenames
    @findInStrings(.sound$ - "wav" + "Pitch" , 0)
    .exists = findInStrings.return
  until .i = total_sounds or !.exists

  if .exists
    @cleanUp()
    exitScript: "No unpaired Sound objects." + newline$
  else
    for_each.start_from = .i
  endif
endproc
