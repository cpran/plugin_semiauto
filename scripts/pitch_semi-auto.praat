#! ---
#! title:  'Semi-auto pitch tracker'
#! author:
#! - 'José Joaquín Atria'
#! - 'Dan McCloy'
#! tags: [praat, cpran, pitch]
#! abstract: |
#!   Inspired by Dan McCloy's "semi-auto" scripts, this script streamlines
#!   the generation and auditing of large numbers of Pitch objects by
#!   providing a "semi-automatic" interface. The user specifies a folder
#!   with sounds (and optionally annotations), and the script iterates
#!   through them, generating a new Pitch object (or opening an existing
#!   one) and giving the user the chance to accept it or modify it.
#!
#!   The script makes use of the facilities in the "vieweach" CPrAN plugin
#!   for making robust wizards, and supports interrupting the process and
#!   resuming it automatically from where it left off. It also uses the
#!   "twopass" CPrAN plugin for per-utterance estimation of f0 ranges.
#! ---
#!
#! This script is heavily inspired by Dan McCloy's Semi-auto Pitch
#! Extractor [@mccloy2012], although it has been almost entirely rewritten
#! to make use of the new syntax as well as the object-cycling features
#! implemented in the "vieweach" CPrAN plugin.
#!
#! Provided with a directory with sound files, the script will loop through
#! each one and provide an interface to adjust the pitch detection
#! parameters used by Praat.
#!
#! The user may choose to open accompanying TextGrid annotations to
#! facilitate navigation. If so, the script queries a series of time points
#! or intervals for f0 values using regular expressions so the user can
#! clearly see if the current pitch settings result in the generation of
#! undefined or otherwise incorrect values, and adjut accordingly.
#!
#! The wizard allows for easy navigation not only through each sound file
#! (using the `<<` and `>>` buttons) but also, if annotations are being
#! used, through any relevant time marks (using the `<` and `>` buttons).
#!
#! Unlike McCloy's original script, this script takes a different approach
#! to user manipulation of Pitch objects. In this case, this is done by
#! providing the user with the chance to enter the Pitch editor for the
#! current Sound object, using the parameters that are chosen at that time.
#! These parameters (specifically, pitch floor and ceiling) can be
#! automatically estimated from the current utterance by means of Hirst and
#! de Looze's two-pass approach [@delooze+2008; @hirst2012].
#!
#! This script was developed with funding from University of Chicago
#! and FONDECyT (#1130720)
#!
#! # License
#!
#! This script is free software: you can redistribute it and/or
#! modify it under the terms of the GNU General Public License as
#! published by the Free Software Foundation, either version 3 of
#! the License, or (at your option) any later version.
#!
#! A copy of the GNU General Public License is available at
#! <http://www.gnu.org/licenses/>.

trace$ = "semiauto_pitch_tracker.log"
trace  = 0

include ../../plugin_semiauto/procedures/semiauto.proc
include ../../plugin_utils/procedures/utils.proc
include ../../plugin_utils/procedures/check_directory.proc
include ../../plugin_utils/procedures/check_filename.proc
include ../../plugin_selection/procedures/tiny.proc
include ../../plugin_twopass/procedures/pitch_two-pass.proc
include ../../plugin_strutils/procedures/find_in_strings.proc

include ../../plugin_config/procedures/config.proc
config.use_table = 1

@normalPrefDir()

strutils$  = preferencesDirectory$ + "/plugin_strutils/scripts/"
selection$ = preferencesDirectory$ + "/plugin_selection/scripts/"

#! # Arguments
#!
#! **Sound directory** (sentence)
#!
#!   : The path to the sound files
#!
#! **TextGrid directory** (sentence)
#!
#!   : The path to the annotations
#!
#! **Tier** (integer)
#!
#!   : The tier in the annotations to be used for the analysis. This can be
#!     an interval or a point tier. Setting it to `0` ignores TextGrids
#!     altogether.
#!
#! **Label regex** (sentence)
#!
#!   : The regular expression to identify labels in the tier. The default
#!     value of `.+` includes all non-empty labels.
#!
#! **Time span (s)** (positive)
#!
#!   : When the target tier is a point tier, the time span specifies the
#!     time window to use when automatically zooming in to each point (ie.
#!     when zoomed in, the window will show the specified number of seconds
#!     centered on the appropriate point).
#!
#! **Measures per mark** (integer)
#!
#!   : To assist in user evaluation of Pitch objects, measures are taken
#!     for matching intervals or points in the specified TextGrid tier. The
#!     value of this variable specifies how many of these measures to make.
#!     With a positive value, the specified number of evenly spaced
#!     measures are made, while a value of `0` gets a mean value for the
#!     interval.
#!
#!     If the specified tier is a point tier, both of these values are the
#!     same.
#!
#! **Output directory** (sentence)
#!
#!   : The path to sve the generated Pitch objects.
#!
#! **Pitch range minimum** (real)
#!
#!   : The bottom of the pre-set pitch range. Setting this value (or the
#!     pitch range maximum) to `0`, results in the pitch range being
#!     detected automatically using the "twopass" plugin.
#!
#! **Pitch range maximum** (real)
#!
#!   : The top of the pre-set pitch range. Setting this value (or the pitch
#!     range minimum) to `0`, results in the pitch range being detected
#!     automatically using the "twopass" plugin.
#!
#! <!-- -->

#! Because of the large amount of variables handled by the script, these
#! are stored using two namespaces: `d`, storing the default values
#! that apply for all iterations; and `l`, storing the local values that
#! apply for each iteration. In each namespace, corresponding variables have
#! similar names.
#!
#! This is necessary because some defaults (like the magnitude of the f0
#! range) can be overriden for specific sounds, and new values can also be
#! set to change the defaults for future iterations.

@initial_defaults()

label SEMIAUTO_FORM_START

@trace: "Assigned default variables"

beginPause: "Semi-auto pitch tracking..."
  comment:  "Leave paths empty for GUI selectors"
  sentence: "Sound directory",    d.sound_directory$
  sentence: "TextGrid directory", d.textgrid_directory$
  sentence: "Tier",               d.tier$
  sentence: "Label_regex",        d.label_regex$
  sentence: "Time span (s)",      d.span$
  sentence: "Measures per mark",  d.measures$
  sentence: "Output directory",   d.output_directory$
  comment:  "Set either to 0 for automatic per-utterance estimation"
  sentence: "left Default pitch range (Hz)",  d.pitch_floor$
  sentence: "right Default pitch range (Hz)", d.pitch_ceiling$
  boolean:  "Overwrite by default", number(d.overwrite$)
form_button = endPause: "Load config file", "Cancel", "OK", 3, 2

if form_button == 2
  exit
endif

#! To make it easier to manage the sets of preferences for the script, and
#! the reproducibility of the execution of the script, all parameters can be
#! read from an optional configuration file, which is specified at startup.
#! And the user has the possibility to also modify any of these parameters
#! after reading them from the configuration file.

if form_button == 1
  @checkFilename: "", "Select config file..."
  d.config$ = checkFilename.name$

  @defaults_from_config()
  goto SEMIAUTO_FORM_START
elsif form_button == 2
  exit
elsif form_button > 3
  exitScript: "Unreachable condition"
endif

@defaults_from_form()

@trace: "Script start"

#! Both input paths, as well as the output path, can be set to blank strings
#! to use instead graphical directory selectors. This makes it possible to
#! use the script programmatically (passing the path as an argument if
#! known), while still making it easy to use by humans.
#!
#! [^1]:
#!     The procedures allowing this are provided by the *utils* CPrAN plugin.

@trace: "Creating full-path Sound list"

#! A list of files for both sounds and annotations are stored as a Strings
#! object with full paths, supporting setups in which they reside in
#! different directories.[^2]
#!
#! [^2]:
#!     The generation of these lists (not natively supported by Praat) is
#!     provided by the *strutils* CPrAN plugin.

runScript: strutils$ + "file_list_full_path.praat",
  ... "semiauto_sounds_fullpath", d.sound_directory$, "*wav", 1
sound_basenames = selected(1)
sound_list      = selected(2)

selectObject: sound_list
total_sounds = Get number of strings

if !total_sounds
  @clean_exit: "No valid sound found in " + d.sound_directory$
endif

@trace: "  Sound check passed"

if d.tier
  @trace: "Creating full-path TextGrid list"

  runScript: strutils$ + "file_list_full_path.praat",
    ... "semiauto_textgrids_fullpath", d.textgrid_directory$, "*TextGrid", 0
  textgrid_list = selected()
  Rename: "textgrid_list"

  selectObject: textgrid_list
  total_textgrids = Get number of strings

  if total_sounds != total_textgrids
    @clean_exit: "Not enough annotations for all sounds"
  endif

  @trace: "  TextGrid check passed"
endif

#! Once the output path has been specified, a list of existing Pitch
#! objects in that directory is created (also using full paths) to detect
#! whether certain sounds already have an associated Pitch object. This not
#! only makes it possible for the script to resume automatically from where
#! it left off, but it also means that backtracking to previously visited
#! sounds does not overwrite Pitch objects that have already been audited.

pitch_basenames = Create Strings as file list: "semiauto_pitch_list",
  ... d.output_directory$ + "*Pitch"
total_pitchs = Get number of strings

@trace: "Checking for exising Pitch objects"

if total_pitchs
  beginPause: "Pitch objects found"
  comment: "The output directory already contains some Pitch objects"
  comment: "Do you want to continue from the first unpaired Sound?"
  button = endPause: "Cancel", "Continue", "From start", 2, 1
  if button = 1
    @cleanup()
    exit
  elsif button = 2
    selectObject: sound_basenames, pitch_basenames
    @find_first_unpaired()
  elsif button = 3
    for_each.start_from = 1
  endif
else
  for_each.start_from = 1
endif

#! Information about the objects that are known to the script is kept in a
#! separate table, allowing the script to save user-specified values for
#! each, and recover them when necessary.

@getTable: d.output_table$, d.output_columns$
Rename: "semiauto_output_table"
d.output_table = getTable.id

@getTable: d.output_table$, d.output_columns$
Rename: "semiauto_output_table"
d.output_table = getTable.id

@trace: "Created object list"

procedure getTable: .filename$, .columns$
  if fileReadable(.filename$)
    @trace: "Found output table at """ + .filename$ + """"

    .id = Read Table from tab-separated file: .filename$
    .c$ = List: 0
    .c$ = tab$ + extractLine$(.c$, "") + tab$
    if .c$ != tab$ + replace$(.columns$, " ", tab$, 0) + tab$
      @trace: "Output table not valid!"
      exit
    endif
  else
    @trace: "Creating new output table"
    .id = Create Table with column names: "got_table", 0,
      ... .columns$
  endif
endproc

#! # Procedure overrides
#!
#! The object cycling features in "vieweach" provide hooks to specify code
#! to be executed at specific points during each iteration. In order to
#! inject that code, specific procedures have to be locally defined before
#! we include the main procedure definitions.
#!
#! Some internal variables are used (they start with "view_each" of
#! "for_each"), but care must be used when modifying them, since this might
#! have unexpected results. Please refer to the "vieweach documentation.

#! ## `view_each.before_editor`
#!
#! ~~~ params
#! in:
#!   d.state_table: >
#!     ID of a Table with a register of objects known to the script
#!   d.use_textgrids: >
#!     True if TextGrids are in use, false otherwise
#!   d.tier: >
#!     The number of the working tier
#!   d.measures: >
#!     Number of measures to show per mark (0 for mean)
#! out:
#!   valid_marks: >
#!     The ID of a Table registering the current marks matching the user query
#!   l.sound: >
#!     ID of current Sound object
#!   l.sound$: >
#!     Name of current Sound object
#!   l.textgrid: >
#!     ID of current TextGrid object, if applicable
#!   l.textgrid$: >
#!     Name of current TextGrid object, if applicable
#!   l.total_marks: >
#!     Total points or intervals of current TextGrid, if applicable.
#!   l.interval_marks: >
#!     True if working tier in TextGrid is interval tier.
#!   l.mark_name$: >
#!     Name of mark type in current working tier (eg. "interval", "point").
#!   l.has_pitch: >
#!     True if there is a Pitch object on disk for the current Sound object
#!   l.pitch$: >
#!     The name of the current Pitch object
#!   l.pitch_floor: >
#!     The currently specified minimum f0 value
#!   l.pitch_ceiling: >
#!     The currently specified maximum f0 value
#!   view_each.editor: >
#!     ID of the current editor
#!   view_each.editor$: >
#!     The name of the current editor
#! selection:
#!   in:
#!     sound: 1
#!     textgrid: 0-1
#! ~~~
#!
#! Executes at the beginning of each iteration, before the editor is open.
#!
procedure view_each.before_editor ()
  @trace: "View each: before editor"

  l.sound = selected("Sound")
  l.sound$ = selected$("Sound")

  @trace: "  View each: processing " + l.sound$

  #! If the script is using annotations (ie. if the `Tier` argument was not
  #! set to `0`), then the appropriate marks (either interval or point labels
  #! matching the initial regular expression) are queried and stored in an
  #! external table (see [find-marks](#find-marks)).

  if d.use_textgrids
    l.textgrid = selected("TextGrid")
    l.textgrid$ = selected$("TextGrid")

    view_each.editor = l.textgrid
    view_each.editor$ = "TextGrid " + l.textgrid$

    selectObject: l.textgrid
    l.interval_marks = Is interval tier: d.tier
    l.mark_name$ = if l.interval_marks then "interval" else "point" fi
    .tiers = Get number of tiers

    # Since we may be applying the analysis to a subset of points on that tier,
    # we keep a list of the valid points for this particular object.
    @find_marks()
    valid_marks = find_marks.table
    l.total_marks = Object_'valid_marks'.nrow

    # We append a point tier for the display of the found pitch values, and
    # populate it with the points we'll need.
    if l.total_marks
      selectObject: l.textgrid
      output_tier = .tiers + 1
      if l.interval_marks and d.measures == 0
        Insert interval tier: output_tier, "f0"
        for .i to l.total_marks
          nocheck Insert boundary: output_tier, Object_'valid_marks'[.i, "start"]
          nocheck Insert boundary: output_tier, Object_'valid_marks'[.i, "end"]
        endfor
      else
        Insert point tier: output_tier, "f0"
        for .i to l.total_marks
          if l.interval_marks
            .start = Object_'valid_marks'[.i, "start"]
            .end   = Object_'valid_marks'[.i, "end"]
            @split_time: .start, .end, d.measures
            for .j to d.measures
              Insert point: output_tier, split_time.time[.j], ""
            endfor
          else
            Insert point: output_tier, Object_'valid_marks'[.i, "mid"], ""
          endif
        endfor
      endif
    endif
  else
    l.total_marks = 0
    view_each.editor  = l.sound
    view_each.editor$ = "Sound " + l.sound$
  endif

  # If we've already "seen" this object, we read the values that were specified
  # for it before. If not, we provide the default ones.
  selectObject: d.state_table
  .exists = Search column: "name", l.sound$
  if !.exists
    l.pitch_floor   = d.pitch_floor
    l.pitch_ceiling = d.pitch_ceiling
    selectObject: d.state_table
    Append row
    Set string value:  Object_'d.state_table'.nrow, "name", l.sound$
  else
    .row = .exists
    l.pitch_floor   = Object_'d.state_table'[.row, "floor"]
    l.pitch_ceiling = Object_'d.state_table'[.row, "ceiling"]
  endif

  # If a Pitch object exists for this sound, then we load that one in. If not,
  # then we use the active settings to generate an initial Pitch object so
  # the values we obtain will remain the same regardless of what the user does
  # with the editor window (which is there just for display).

  @pitch_exists(l.sound$ + ".Pitch")
  l.has_pitch = pitch_exists.return
  l.pitch$    = pitch_exists.return$ - ".Pitch"
  if l.has_pitch
    l.pitch = Read from file: d.output_directory$ + l.pitch$ + ".Pitch"
  else
    @make_pitch()
  endif
  @get_measures()

  # We'll be testing this button later, so we initialise this variable.
  view_each.pause.button$ = "--undefined--"

  selectObject: for_each.selection
  runScript: selection$ + "restore_selection.praat"

endproc ; view_each.before_editor

#! ## `view_each.at_begin_editor`
#!
#! ~~~ params
#! in:
#!   l.pitch_floor: >
#!     The currently specified minimum f0 value
#!   l.pitch_ceiling: >
#!     The currently specified maximum f0 value
#! selection:
#!   in:
#!     sound: 1
#!     textgrid: 0-1
#! context: editor
#! ~~~
#!
#! Executes at the beginning of the editor window, for each iteration
#!
procedure view_each.at_begin_editor ()
  @trace: "View each: at begin editor"

  Show analyses: "yes", "yes", "no", "no", "no", 10
  Spectrogram settings: 0, 2000, 0.025, 50
  Advanced spectrogram settings: 1000, 250,
    ... "Fourier", "Gaussian", "yes", 100, 6, 0
  Pitch settings: l.pitch_floor, l.pitch_ceiling,
    ... "Hertz", "autocorrelation", "automatic"
  Advanced pitch settings: 0, 0, "no", 15, 0.03, 0.45, 0.01, 0.35, 0.14
endproc ; view_each.at_begin_editor

#! ## `view_each.pause`
#!
#! ~~~ params
#! in:
#!   d.state_table: >
#!     ID of a Table with a register of objects known to the script
#!   pitch_basenames: >
#!     ID of a Table with Pitch objects existing on disk
#!   d.overwrite: >
#!     Global flag for overwriting Pitch objects on disk
#!   l.total_marks: >
#!     Total points or intervals of current TextGrid, if applicable.
#!   l.sound$: >
#!     Name of current Sound object
#!   l.has_pitch: >
#!     True if there is a Pitch object on disk for the current Sound object
#!   l.pitch$: >
#!     The name of the current Pitch object
#!   l.pitch_floor: >
#!     The currently specified minimum f0 value
#!   l.pitch_ceiling: >
#!     The currently specified maximum f0 value
#!   l.mark_name$: >
#!     Name of mark type in current working tier (eg. "interval", "point").
#!   view_each.editor$: >
#!     The name of the current editor, provided by view_each
#!   for_each.current: >
#!     The index number of the current object, from for_each
#!   for_each.last: >
#!     The index number of the last object, from for_each
#! selection:
#!   in:
#!     sound: 1
#!     textgrid: 0-1
#! ~~~
#!
#! Defines the pause that occurs at each iteration
#! This is where most of the logic for this script is defined.
#!
#! Depending on user input, the values of `d.pitch_floor` and
#! `d.pitch_ceiling` may be modified.
#!
procedure view_each.pause ()
  @trace: "View each: pause"

  # .mark and .marks keep track of how many "valid" points this
  # object pair has, and which of those points (if any) is the one that is
  # being displayed.
  # This is used because the script provides the user with the possibility of
  # iterating through objects, and through points within each object.
  .mark = 0
  .marks = l.total_marks

  # The Pause menu is within a repeat block so that the user can redraw the
  # contents of the editor with new settings until they are satisfied.
  repeat
    label SEMIAUTO_PAUSE_START
    selectObject: d.state_table
    .object_row = Search column: "name", l.sound$

    # If we are repeating this block, then we recreate the Pitch object
    if l.has_pitch
      Set string value: .object_row, "pitch", l.pitch$
    elsif .button$ == "redo" or !l.pitch_floor or !l.pitch_ceiling
      @make_pitch()
      @get_measures()
      editor: view_each.editor$
        Pitch settings: l.pitch_floor, l.pitch_ceiling,
          ... "Hertz", "cross-correlation", "automatic"
      endeditor
    endif
    .button$ = ""

    # If we are displaying a specific point, then adjust the zoom.
    # If not, then show the entire sound.
    if .mark
      editor: view_each.editor$
        Zoom: Object_'valid_marks'[.mark, "start"],
          ... Object_'valid_marks'[.mark, "end"]
      endeditor
    endif

    .message$ = "Sound " + l.sound$ + " " +
      ... "(" + string$(for_each.current) + " of " + string$(for_each.last) + ")"
    if .mark
      .message$ = .message$ +
        ... if .mark then " - " + l.mark_name$ + " " +
          ... string$(.mark) + " of " + string$(.marks)
        ... else "" fi
    endif

    beginPause: "Adjust F0 analysis settings"
    comment: .message$
    if l.has_pitch
      comment: "There is already a Pitch object for this file. " +
        ... "Press Edit to view."
    else
      comment: "You can change the pitch settings " +
        ... "if the F0 track doesn't look right."
    endif
    real: "Pitch_floor", l.pitch_floor
    real: "Pitch_ceiling", l.pitch_ceiling
    boolean: "Set as default", 0
    if l.has_pitch
      comment: "Press Reset to ignore existing Pitch object"
    endif
    if .marks
      if get_measures.all_defined
        comment: "(All valid "     + l.mark_name$ + "s have defined F0 values)"
      else
        comment: "(Not all valid " + l.mark_name$ + "s have defined F0 values!)"
      endif
    endif

    .stop = 1
    .back$       = if for_each.current > 1 then """""<<"""", " else "" fi
    if .marks
      .prev_mark$ = """""<"""", "
      .next_mark$ = """"">"""", "
    else
      .prev_mark$ = ""
      .next_mark$ = ""
    endif
    .redo$       = if l.has_pitch then
      ... """""Reset""""" else """""Redraw""""" fi + ", "
    .edit$       = """""Edit"""", "
    .forward$    = if for_each.current = for_each.last then
      ... """""Finish""""" else """"">>""""" fi
    .buttons$ = """Stop"", " +
      ... " '.back$' '.prev_mark$' " +
      ... " '.redo$' '.edit$' " +
      ... " '.next_mark$' '.forward$'"

    .stop       = 1
    .back       = 0
    if .marks
      .prev_mark = 1
      .next_mark = 1
    else
      .prev_mark = 0
      .next_mark = 0
    endif
    .redo       = 1
    .edit       = 1
    .forward    = 1
    if for_each.current > 1
      .back = 1
    endif

    .button_counter = 1
    if .back
      .button_counter += 1
      .back = .button_counter
    endif
    if .prev_mark
      .button_counter += 1
      .prev_mark = .button_counter
    endif
    .button_counter += 1
    .redo = .button_counter
    .button_counter += 1
    .edit = .button_counter
    if .next_mark
      .button_counter += 1
      .next_mark = .button_counter
    endif
    .button_counter += 1
    .forward = .button_counter

    .button = endPause: '.buttons$', .button_counter, 1

    if .button != .stop
      l.pitch_floor   = pitch_floor
      l.pitch_ceiling = pitch_ceiling

      if set_as_default
        d.pitch_floor   = l.pitch_floor
        d.pitch_ceiling = l.pitch_ceiling
      endif

      if .button = .redo
        .mark = 0
        .button$ = "redo"
        l.has_pitch = 0
        nocheck removeObject: l.pitch
      elsif .button = .next_mark or .button = .prev_mark
        .mark += if .button = .next_mark then 1 else -1 fi
        .mark = .mark mod (.marks + 1)
        if !.mark
          .mark += if .button = .next_mark then 1 else -1 fi
        endif
        .mark = if .mark < 0 then
          ... .marks else .mark fi
        goto SEMIAUTO_PAUSE_START
      elsif .button = .edit
        selectObject: l.pitch
        View & Edit
        beginPause: "Edit Pitch object"
          comment: "Press Keep to keep changes, or Discard to remove object"
        .pitch_button = endPause: "Discard", "Keep", 2, 1
        nocheck editor: "Pitch " + l.pitch$
          nocheck Close
        nocheck endeditor
        if .pitch_button = 1
          nocheck removeObject: l.pitch
          l.pitch = undefined
          l.has_pitch = 0
          @make_pitch()
        endif
        @get_measures()
        goto SEMIAUTO_PAUSE_START
      endif
    endif

  # End of pause block
  until .button != .redo

  if .button != .stop
    selectObject: d.state_table
    Set numeric value: .object_row, "floor",   l.pitch_floor
    Set numeric value: .object_row, "ceiling", l.pitch_ceiling

    if .button = .forward and l.pitch != undefined
      selectObject: l.pitch
      if fileReadable(d.output_directory$ + l.pitch$ + ".Pitch") and !d.overwrite
        beginPause: "File exists. Overwrite?"
        .overwrite = endPause: "No", "Yes", 1, 1
        .overwrite -= 1
      else
        .overwrite = 1
      endif
      if .overwrite
        Save as text file: d.output_directory$ + l.pitch$ + ".Pitch"
      endif
      Remove

      @pitch_exists(l.pitch$ + ".Pitch")
      if !pitch_exists.return
        selectObject: pitch_basenames
        Insert string: 1, l.pitch$
      endif

      selectObject: d.state_table
      Set string value: .object_row, "pitch", l.pitch$
    endif
  endif

  # The for_each() procedure in "vieweach" expects a .next variable with
  # the item to be shown next. This variable can have one of three possible
  # values:
  #   * -1, to go to the previous object
  #   * +1, to go to the next object
  #   *  0, to exit
  if .button = .stop
    # Pressed stop
    for_each.next = 0
  elsif .button = .back
    # Pressed back
    for_each.next = -1
  elsif .button = .forward
    # Pressed forward
    for_each.next = if for_each.current = for_each.last then 0 else 1 fi
  endif
endproc ; view_each.pause

#! ## `view_each.at_end_iteration`
#!
#! ~~~ params
#! in:
#!   d.use_textgrids: >
#!     True if TextGrids are in use, false otherwise
#!   valid_marks: >
#!     The ID of a Table registering the current marks matching the user query
#!   l.pitch: >
#!     The ID of the current Pitch object
#! selection:
#!   in:
#!     sound: 1
#!     textgrid: 0-1
#! ~~~
#!
#! Called at the end of each iteration.
#!
procedure view_each.at_end_iteration ()
  @trace: "View each: at end iteration"

  if d.use_textgrids
    removeObject: valid_marks
  endif
  nocheck removeObject: l.pitch
endproc ; view_each.at_end_iteration

@trace: "Include vieweach"
include ../../plugin_vieweach/procedures/view_each.proc

# Call the main procedure
@trace: "Run view each"
selectObject: sound_list
if d.use_textgrids
  plusObject: textgrid_list
endif
@view_each()

@trace: "Clean up"
# Clean up.
@cleanup()

#!
#! ## Internal procedures
#!
#! These procedures are only used for this script
#!

#! ## `get_measures` {#get-measures}
#!
#! ~~~ params
#! in:
#!   view_each.editor$: >
#!     The name of the current editor, provided by view_each
#!   d.use_textgrids: >
#!     True if TextGrids are in use, false otherwise
#!   d.measures: >
#!     Number of measures to show per mark (0 for mean)
#!   l.sound: >
#!     ID of current Sound object
#!   l.pitch: >
#!     ID of current Pitch object
#!   l.textgrid: >
#!     ID of current TextGrid object, if applicable
#!   l.total_marks: >
#!     Total points or intervals of current TextGrid, if applicable.
#!   l.interval_marks: >
#!     True if working tier in TextGrid is interval tier.
#!   l.mark_name$: >
#!     Name of mark type in current working tier (eg. "interval", "point").
#!   output_tier: The tier used to output the measures
#! out:
#!   .all_defined: >
#!     True if current parameters resulted in no undefined values.
#! ~~~
#!
#! Populates the current output tier with measures for the current objects
#! using the specified parameters.
#!
#! Can be called regardless of whether inside or outside of an editor
#! environment.
#!
procedure get_measures ()
  if d.use_textgrids
    nocheck editor: view_each.editor$
      @in_editor()
      if in_editor.return
        Close
      endif
    nocheck editor

    selectObject: l.textgrid
    .tiers = Get number of tiers

    .all_defined = 1
    for .r to l.total_marks
      .time  = Object_'valid_marks'[.r, "mid"]

      selectObject: l.pitch
      if !l.interval_marks
        @make_measures: .time, .time, 1
      else
        .start = Object_'valid_marks'[.r, "start"]
        .end   = Object_'valid_marks'[.r, "end"]
        @make_measures: .start, .end, d.measures
      endif

      if !make_measures.all_defined
        .all_defined = 0
      endif

      selectObject: l.textgrid
      if l.interval_marks and d.measures == 0
        # We are recording means
        .mark = Get interval at time: output_tier, .time
        Set interval text: output_tier, .mark, fixed$(make_measures.return[1], 2)
      else
        # We are recording multiple measures
        for .m to d.measures
          .mark = Get nearest index from time:
            ... output_tier, split_time.time[.m]
          Set point text: output_tier, .mark,
            ... fixed$(make_measures.return[.m], 2)
        endfor
      endif
    endfor

    if in_editor.return
      selectObject: l.sound, l.textgrid
      View & Edit
    endif
  endif
endproc ; get_measures

#! ## `make_measures` {#make-measures}
#!
#! ~~~ params
#! in:
#!   .start: The start point of the interval
#!   .end: The end point of the interval
#!   .n: The number of measres to make (0 for a single mean)
#! out:
#!   .return[]: The list of evenly spaced measures
#!   .all_defined: True if all measures are defined
#! ~~~
#!
#! Makes `.n` evenly spaced f0 measures between `.start` and `.end`.
#!
procedure make_measures: .start, .end, .n
  .pitch = selected("Pitch")
  .all_defined = 1
  if .start != .end and .n == 0
    .length = 1
    .time[1] = (.end - .start) / 2 + .start
    .return[1] = Get mean: .start, .end, "Hertz"
  else
    @split_time: .start, .end, .n
    .length = .n
    for .i to .n
      .return[.i]= Get value at time:
        ... split_time.time[.i], "Hertz", "Linear"
    endfor
  endif
  for .i to .length
    if .return[.i] == undefined
      .all_defined = 0
    endif
  endfor
endproc ; make_measures

#! ## `clean_exit` {#clean-exit}
#!
#! Called to interrupt the execution of the script without polluting the
#! Objects list.
#!
procedure clean_exit: .message$
  @cleanup()
  @view_each.clean_exit: .message$
endproc ; clean_exit

#! ## `make_pitch` {#make-pitch}
#!
#! ~~~ params
#! out:
#!   l.pitch: >
#!     ID of Pitch object generated for this iteration. If a Pitch
#!     object already existed, it is replaced.
#! internal: true
#! ~~~
#!
#! Generates a Pitch object for the current sound, using the currently
#! specified values.If a Pitch object has already been created (either from
#! scratch or reading from the filesystem), the procedure replaces it with
#! a new one.
#!
#! Can be called regardless of whether inside or outside of an editor
#! environment.
#!
procedure make_pitch ()
  @in_editor()
  if in_editor.return
    endeditor
  endif

  if variableExists("l.pitch")
    if l.pitch != undefined and l.pitch
      nocheck removeObject: l.pitch
    endif
  endif

  selectObject: l.sound
  if !l.pitch_floor or !l.pitch_ceiling
    @pitchTwoPass(0.75, 1.5)
    l.pitch = selected("Pitch")
    l.pitch_floor   = pitchTwoPass.floor
    l.pitch_ceiling = pitchTwoPass.ceiling
  else
    l.pitch = To Pitch: 0, l.pitch_floor, l.pitch_ceiling
  endif
  l.pitch$ = selected$("Pitch")

  @get_measures()

  if in_editor.return
    editor: in_editor.return$
  endif
endproc ; make_pitch

#! ## `cleanup` {#cleanup}
#!
#! Safely remove all objects created per iteration.
#!
procedure cleanup ()
  nocheck removeObject: l.pitch
  nocheck removeObject: l.sound
  nocheck removeObject: l.textgrid
  nocheck removeObject: sound_basenames
  nocheck removeObject: sound_list
  nocheck removeObject: textgrid_list
  nocheck removeObject: pitch_basenames
  nocheck removeObject: d.state_table
endproc ; cleanup

#! ## `pitch_exists` {#pitch-exists}
#!
#! ~~~ params
#! in:
#!   .name$: The name of the Pitch object to query
#! out:
#!   .return: ID of Pitch object if it exists. Undefined otherwise.
#!   .return$: Name of Pitch object if it exists. Undefined otherwise.
#! internal: true
#! ~~~
#!
#! Find a Pitch object by name, or return `undefined` if it doesn't exist.
#!
procedure pitch_exists (.name$)
  selectObject: pitch_basenames
  @findInStrings: .name$, 0
  .return = findInStrings.return
  if .return
    .return$ = Get string: .return
  else
    .return$ = "--undefined--"
  endif
endproc ; pitch_exists

#
# Procedures to handle default values
#

#! ## `_initialise_defaults` {#initialise-defaults}
#!
#! ~~~ params
#! out: Modifies values in the default namespace
#! internal: true
#! ~~~
#!
#! Sets the variables in the default namespace that do not depend on other
#! variables
#!
procedure _initialise_defaults ()
  d.pitch_floor$        = "75"
  d.pitch_ceiling$      = "600"
  d.use_textgrids$      = "1"
  d.tier$               = "1 (0 = ignore TextGrids)"
  d.overwrite$          = "1"
  d.label_regex$        = ".+"
  d.span$               = "0.2"
  d.measures$           = "0 (= use mean)"
  d.sound_directory$    = ""
  d.textgrid_directory$ = ""
  d.output_directory$   = ""
  d.state_columns$      = "name floor ceiling pitch edited"
  d.output_columns$     = "name index label measure value"
  d.config_name$        = "pitch_semi-auto.conf"
  d.state_name$         = ".pitch_semi-auto_state.csv"
  d.output_name$        = "pitch_semi-auto.csv"
endproc ; _initialise_defaults

#! ## `defaults_from_config` {#defaults-from-config}
#!
#! ~~~ params
#! out: Modifies values in the default namespace
#! internal: true
#! ~~~
#!
#! Read defaults from the config file if it exists
#!
procedure defaults_from_config ()
  @saveSelection()
  @config: d.config$
  .config = selected("Table")

  if do("Get column index...", "pitch floor")
    d.pitch_floor$ = Object_'.config'$[1, "pitch floor"]
    @trace: "[config] pitch floor = " + d.pitch_floor$
  endif

  if do("Get column index...", "pitch ceiling")
    d.pitch_ceiling$ = Object_'.config'$[1, "pitch ceiling"]
    @trace: "[config] pitch ceiling = " + d.pitch_ceiling$
  endif

  if do("Get column index...", "use textgrids")
    d.use_textgrids$ = Object_'.config'$[1, "use textgrids"]
    @trace: "[config] use_textgrids = " + d.use_textgrids$
  endif

  if do("Get column index...", "tier")
    d.tier$ = Object_'.config'$[1,"tier"]
    @trace: "[config] tier = " + d.tier$
  endif

  if do("Get column index...", "overwrite")
    d.overwrite$ = Object_'.config'$[1, "overwrite"]
    @trace: "[config] overwrite = " + d.overwrite$
  endif

  if do("Get column index...", "span")
    d.span$ = Object_'.config'$[1, "span"]
    @trace: "[config] span = " + d.span$
  endif

  if do("Get column index...", "measures")
    d.measures$ = Object_'.config'$[1, "measures"]
    @trace: "[config] measures = " + d.measures$
  endif

  if do("Get column index...", "label regex")
    d.label_regex$ = Object_'.config'$[1, "label regex"]
    @trace: "[config] label regex = " + d.label_regex$
  endif

  if do("Get column index...", "sound directory")
    .dir$ = Object_'.config'$[1, "sound directory"]
    @relative_or_absolute: .dir$, left$(d.config$, rindex(d.config$, "/"))
    d.sound_directory$ = relative_or_absolute.return$
    @trace: "[config] sound directory = " + d.sound_directory$
  endif

  if do("Get column index...", "textgrid directory")
    .dir$ = Object_'.config'$[1, "textgrid directory"]
    @relative_or_absolute: .dir$, left$(d.config$, rindex(d.config$, "/"))
    d.textgrid_directory$ = relative_or_absolute.return$
    @trace: "[config] textgrid directory = " + d.textgrid_directory$
  endif

  if do("Get column index...", "output directory")
    .dir$ = Object_'.config'$[1, "output directory"]
    @relative_or_absolute: .dir$ , left$(d.config$, rindex(d.config$, "/"))
    d.output_directory$ = relative_or_absolute.return$
    @trace: "[config] output directory = " + d.output_directory$
  endif

  removeObject: .config
  @restoreSelection()
endproc ; defaults_from_config

#! ## `_string_defaults` {#string-defaults}
#!
#! ~~~ params
#! out: Modifies values in the default namespace
#! internal: true
#! ~~~
#!
#! Assign string defaults from existing values
#!
procedure _string_defaults ()
  d.output_table$ = d.output_directory$ + d.output_name$
  d.state_table$  = d.output_directory$ + d.state_name$
  d.config$       =                       d.config_name$
endproc ; _string_defaults

#! ## `_numeric_defaults` {#numeric-defaults}
#!
#! ~~~ params
#! out: Modifies values in the default namespace
#! internal: true
#! ~~~
#!
#! Assign numeric defaults from existing values
#!
procedure _numeric_defaults ()
  d.pitch_floor   = number(d.pitch_floor$)
  d.pitch_ceiling = number(d.pitch_ceiling$)
  d.use_textgrids = number(d.use_textgrids$)
  d.tier          = number(d.tier$)
  d.overwrite     = number(d.overwrite$)
  d.label_regex   = number(d.label_regex$)
  d.span          = number(d.span$)
  d.measures      = number(d.measures$)
endproc ; _numeric_defaults

#! ## `defaults_from_form` {#defaults-from-form}
#!
#! ~~~ params
#! out: Modifies values in the default namespace
#! internal: true
#! ~~~
#!
#! Updates variables in the default namespace with values from the
#! initial form
#!
procedure defaults_from_form ()
  d.pitch_floor$    = left_Default_pitch_range$
  d.pitch_ceiling$  = right_Default_pitch_range$
  d.use_textgrids$  = tier$
  d.tier$           = tier$
  d.overwrite$      = string$(overwrite_by_default)
  d.label_regex$    = label_regex$
  d.span$           = time_span$
  d.measures$       = measures_per_mark$
  d.output_columns$ = "name floor ceiling pitch notes"

  @checkDirectory(sound_directory$, "Read Sounds from...")
  d.sound_directory$ = checkDirectory.name$

  @checkDirectory(output_directory$, "Save Pitch objects to...")
  d.output_directory$ = checkDirectory.name$

  if d.use_textgrids
    @checkDirectory(textGrid_directory$, "Read TextGrids from...")
    d.textgrid_directory$ = checkDirectory.name$
  endif

  @set_defaults()
endproc ; defaults_from_form

#!
#! ## References
#!
#! ---
#! link-citations: true
#! references:
#! - title: Detecting changes in key and range for the automatic modelling
#!     and coding of intonation
#!   author:
#!   - given: Céline
#!     family: De Looze
#!   - given: Daniel J.
#!     family: Hirst
#!   id: delooze+2008
#!   issued:
#!     date-parts:
#!     - - 2008
#!   type: paper-conference
#!   container-title: Speech prosody
#!   accessed:
#!     date-parts:
#!     - - 2014
#!       - 10
#!       - 20
#!   URL: http://sprosig.isle.illinois.edu/sp2008/papers/id148.pdf
#! - title: 'The analysis by synthesis of speech melody: From data to
#!     models'
#!   title-short: The analysis by synthesis of speech melody
#!   author:
#!   - given: Daniel J.
#!     family: Hirst
#!   id: hirst2012
#!   issued:
#!     date-parts:
#!     - - 2012
#!   type: article-journal
#!   container-title: Journal of Speech Sciences
#!   accessed:
#!     date-parts:
#!     - - 2014
#!       - 3
#!       - 5
#!   issue: '1'
#!   ISSN: '2236-9740'
#!   volume: '1'
#!   page: '55-83'
#!   URL: http://www.journalofspeechsciences.org/index.php/journalofspeechsciences/article/view/21
#! - title: praat-semiauto
#!   author:
#!   - given: Daniel
#!     family: McCloy
#!   id: mccloy2012
#!   issued:
#!     date-parts:
#!     - - 2012
#!   type: webpage
#!   accessed:
#!     date-parts:
#!     - - 2016
#!       - 3
#!       - 11
#!   URL: https://github.com/drammock/praat-semiauto
#! ---
