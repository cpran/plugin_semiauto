# semiauto setup
#
# This script is part of the semiauto CPrAN plugin for Praat.
# The latest version is available through CPrAN or at
# <http://cpran.net/plugins/semiauto>
#
# The semiauto plugin is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# The semiauto plugin is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with utils. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2014-2016 Dan McCloy, Jose Joaquin Atria

nocheck Add menu command: "Objects", "Praat", "semiauto", "CPrAN", 1, ""

nocheck Add menu command: "Objects", "Praat", "Pitch semi-auto...",   "semiauto", 2, "scripts/pitch_semi-auto.praat"
nocheck Add menu command: "Objects", "Praat", "Formant semi-auto...", "semiauto", 2, "scripts/formant_semi-auto.praat"
