semiauto
========

Description
-----------

This plugin is an update of Dan McCloy's [original semi-auto scripts][1]
to allow for user intervention and monitoring of Praat's automated
analyses. Only the scripts marked below have been implemented at this
point:

  * [X] Pitch extractor
  * [ ] Formant extractor
  * [ ] Pitch analysis
  * [ ] PitchTier extractor 

From [the original plugin][1]:

> This collection of scripts is designed to maximize the 
> efficiency of hand-measurement in Praat. Many Praat scripts 
> are designed to operate automatically, by taking 
> measurements from all files in a folder, using the same 
> settings for all files, and never showing the user what it's 
> doing until it spits out the finished spreadsheet. For some 
> analyses (like amplitude/intensity measurements) that 
> approach works fine, but in other cases there is an 
> inevitable risk of bad data due to formant- or 
> pitch-tracking errors. These scripts take a different 
> approach, forcing the user to look at the spectrogram and 
> Praat's overlaid pitch or formant track for every 
> measurement. The user's job is to affirm that Praat's 
> algorithms are finding what they're meant to find, and if 
> not, to tweak the settings until the algorithm returns the 
> correct response. Everything else is handled by the scripts: 
> opening files, zooming, placing the cursor, etc. The hope is 
> that what the researcher loses in having to "babysit" the 
> script is gained back in her confidence that the data are 
> accurate and clean: no pitch halving or doubling errors, no 
> spurious formants between F1 and F2, no systematically 
> errorful values for certain talkers due to inappropriate 
> settings, etc.

Requirements
------------

* [`testsimple`](https://gitlab.com/cpran/plugin_testsimple)
* [`utils`](https://gitlab.com/cpran/plugin_utils)
* [`strutils`](https://gitlab.com/cpran/plugin_strutils)
* [`twopass`](https://gitlab.com/cpran/plugin_twopass)
* [`vieweach`](https://gitlab.com/cpran/plugin_vieweach)
* [`selection`](https://gitlab.com/cpran/plugin_selection)

[1]: https://github.com/drammock/praat-semiauto
